const fs = require('fs');
const {nanoid} = require('nanoid');
const filename = './itemDb.json';
const fileCategory = './categoryDb.json';
const filePlace = './placeDb.json';

let data = [];
let categoryData = [];
let placeData = [];

module.exports = {
	init() {
		try {
			const fileContents = fs.readFileSync(filename);
			data = JSON.parse(fileContents);
		} catch (e) {
			data = [];
		}
	},
	getItems() {
		return data;
	},
	getItem(id) {
		return data.find(i => i.id === id);
	},
	addItem(item) {
		try {
			const categoryContents = fs.readFileSync(fileCategory);
			categoryData = JSON.parse(categoryContents);
			const placeContents = fs.readFileSync(filePlace);
			placeData = JSON.parse(placeContents);
		} catch (e) {
			categoryData = [];
			placeData = [];
		}

		let thisCategoryId = false;
		let thisPlaceID = false;

		categoryData.forEach(category => {
			if (category.id === item.category_id) {
				thisCategoryId = true;
			}
		});

		placeData.forEach(place => {
			if (place.id === item.place_id) {
				thisPlaceID = true;
			}
		});

		if ((thisCategoryId === false) && (thisPlaceID === false)){
			return {"error": "Category id, Place id not found"}
		} else if (thisCategoryId === false) {
			return {"error": "Category id not found"}
		} else if (thisPlaceID === false) {
			return {"error": "Place id not found"}
		} else if ((thisCategoryId === true) && (thisPlaceID === true)){
			item.id = nanoid();
			data.push(item);
			this.save();
			return item
		}
	},
	updateItem(id, newItem) {
		const itemIndex = data.findIndex(i => i.id === id);
		Object.keys(data[itemIndex]).map((k, i) => {
			if (k === Object.keys(newItem)[i]) {
				data[itemIndex][k] = newItem[k]
			}
		});
		this.save();
		return data;
	},
	deleteItem(id) {
			const itemIndex = data.findIndex(i => i.id === id);
			data.splice(itemIndex, 1);
			this.save();
			return {"delete": "Deleted"};
	},
	save() {
		fs.writeFileSync(filename, JSON.stringify(data));
	}
};