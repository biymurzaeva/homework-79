const fs = require('fs');
const {nanoid} = require('nanoid');
const filename = './categoryDb.json';
const fileItem = './itemsDb.json';

let data = [];
let itemsData = [];

module.exports = {
	init() {
		try {
			const fileContents = fs.readFileSync(filename);
			data = JSON.parse(fileContents);
		} catch (e) {
			data = [];
		}
	},
	getCategories() {
		return data;
	},
	getCategory(id) {
		return data.find(i => i.id === id);
	},
	deleteCategory(id) {
		try {
			const itemContents = fs.readFileSync(fileItem);
			itemsData = JSON.parse(itemContents);
		} catch (e) {
			itemsData = [];
		}

		const categoryIndex = data.findIndex(i => i.id === id);

		let connectionWithItem = false;

		itemsData.forEach(p => {
			if (p.category_id === data[categoryIndex].id) {
				connectionWithItem  = true;
			}
		});

		if (connectionWithItem === false) {
			data.splice(categoryIndex, 1);
			this.save();
			return data;
		} else if (connectionWithItem === true) {
			return {"connection": "Connection with Items. Unable to remove"};
		}
	},
	addCategory(category) {
		category.id = nanoid();
		data.push(category);
		this.save();
		return category;
	},
	updateCategory(id, category) {
		const categoryIndex = data.findIndex(i => i.id === id);
		Object.keys(data[categoryIndex]).map((k, i) => {
			if (k === Object.keys(category)[i]) {
				data[categoryIndex][k] = category[k]
			}
		});
		this.save();
		return data[categoryIndex];
	},
	save() {
		fs.writeFileSync(filename, JSON.stringify(data));
	}
};