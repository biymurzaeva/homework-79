const fs = require('fs');
const {nanoid} = require('nanoid');
const filename = './placeDb.json';
const fileItem = './itemDb.json';

let data = [];
let itemsData = [];

module.exports = {
	init() {
		try {
			const fileContents = fs.readFileSync(filename);
			data = JSON.parse(fileContents);
		} catch (e) {
			data = [];
		}
	},
	getPlaces() {
		return data;
	},
	getPlace(id) {
		return data.find(i => i.id === id);
	},
	addPlace(place) {
		place.id = nanoid();
		data.push(place);
		this.save();
		return place;
	},
	deletePlace(id) {
		try {
			const itemContents = fs.readFileSync(fileItem);
			itemsData = JSON.parse(itemContents);
		} catch (e) {
			itemsData = [];
		}

		const placeIndex = data.findIndex(i => i.id === id);

		let connectionWithItem = false;

		itemsData.forEach(p => {
			if (p.place_id === data[placeIndex].id) {
				connectionWithItem  = true;
			}
		});

		if (connectionWithItem === false) {
			data.splice(placeIndex, 1);
			this.save();
			return data;
		} else if (connectionWithItem === true) {
			return {"connection": "Connection with Items. Unable to remove"}
		}
	},
	updatePlace(id, place) {
		const placeIndex = data.findIndex(i => i.id === id);
		Object.keys(data[placeIndex]).map((k, i) => {
			if (k === Object.keys(place)[i]) {
				data[placeIndex][k] = place[k]
			}
		});
		this.save();
		return data[placeIndex];
	},
	save() {
		fs.writeFileSync(filename, JSON.stringify(data));
	}
};