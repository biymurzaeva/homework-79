const express = require('express');
const filePlacesDb = require('../filePlacesDb');

const router = express.Router();

router.get('/', (req, res) => {
	const categories = filePlacesDb.getPlaces();
	res.send(categories);
});

router.get('/:id', (req, res) => {
	const place  = filePlacesDb.getPlace(req.params.id);
	if (!place) {
		return res.status(404).send({error: 'Place not found'});
	}

	res.send(place);
});

router.post('/', (req, res) => {
	if (!req.body.name) {
		return res.status(404).send({error: "Data not valid"});
	}

	const place = {
		name: req.body.name,
		description: req.body.description,
	};

	const newPlace = filePlacesDb.addPlace(place);

	res.send(newPlace);
});

router.delete('/:id', (req, res) => {
	filePlacesDb.deletePlace(req.params.id);
	res.send();
});

router.put('/:id', (req, res) => {
	if (!req.body.name) {
		return res.status(404).send({error: "Data not valid"});
	}

	const newPlace = {
		name: req.body.name,
		description: req.body.description,
	};

	const updatePlace = filePlacesDb.updatePlace(req.params.id, newPlace);

	res.send(updatePlace);
});

module.exports = router;