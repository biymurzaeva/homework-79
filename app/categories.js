const express = require('express');
const fileCategoriesDb = require('../fileCategoriesDb');

const router = express.Router();

router.get('/', (req, res) => {
	const categories = fileCategoriesDb.getCategories();
	res.send(categories);
});

router.delete('/:id', (req, res) => {
	fileCategoriesDb.deleteCategory(req.params.id);
	res.send();
});

router.get('/:id', (req, res) => {
	const category  = fileCategoriesDb.getCategory(req.params.id);
	if (!category) {
		return res.status(404).send({error: 'Category not found'});
	}

	res.send(category);
});

router.post('/', (req, res) => {
	if (!req.body.name) {
		return res.status(404).send({error: "Data not valid"});
	}

	const category = {
		name: req.body.name,
		description: req.body.description,
	};

	const newCategory = fileCategoriesDb.addCategory(category);

	res.send(newCategory);
});

router.put('/:id', (req, res) => {
	if (!req.body.name) {
		return res.status(404).send({error: "Data not valid"});
	}

	const newCategory = {
		name: req.body.name,
		description: req.body.description,
	};

	const updateCategory = fileCategoriesDb.updateCategory(req.params.id, newCategory);

	res.send(updateCategory);
});

module.exports = router;