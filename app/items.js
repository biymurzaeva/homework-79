const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const {nanoid} = require('nanoid');
const fileItemsDb = require('../fileItemsDb');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);

	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
	const items = fileItemsDb.getItems();
	res.send(items);
});

router.get('/:id', (req, res) => {
	const item  = fileItemsDb.getItem(req.params.id);
	if (!item) {
		return res.status(404).send({error: 'Item not found'});
	}

	res.send(item);
});

router.post('/', upload.single('image'), (req, res) => {
	if (!req.body.name || !req.body.place_id || !req.body.category_id) {
		return res.status(404).send({error: "Data not valid"});
	}

	const item = {
		category_id: req.body.category_id,
		place_id: req.body.place_id,
		name: req.body.name,
		description: req.body.description,
	};

	if (req.file) {
		item.image = req.file.filename;
	}

	const newItem = fileItemsDb.addItem(item);

	res.send(newItem);
});

router.delete('/:id', (req, res) => {
	const items = fileItemsDb.deleteItem(req.params.id);
	res.send(items);
});

router.put('/:id', (req, res) => {
	if (!req.body.name || !req.body.place_id || !req.body.category_id) {
		return res.status(404).send({error: "Data not valid"});
	}

	const newItem = {
		category_id: req.body.category_id,
		place_id: req.body.place_id,
		name: req.body.name,
		description: req.body.description,
	};

	const updateItem = fileItemsDb.updateItem(req.params.id, newItem);

	res.send(updateItem);
});

module.exports = router;